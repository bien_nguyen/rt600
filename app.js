if (Meteor.isClient) {
	angular.module('rt600', [
		'angular-meteor',
		'ngMaterial',
		'ui.router'
	]);

	angular.module('rt600').config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
		$locationProvider.html5Mode(true);

		$stateProvider
			.state('mainpage', {
				url: '/',
				template: '<main-page></main-page>'
			})
			.state('mio1', {
				url:'/mio1',
				template: '<mio1></mio1>'
			})
			.state('ipc', {
				url:'/ipc',
				template: '<ipc></ipc>'
			});

		$urlRouterProvider.otherwise("/");
	});

	angular.module('rt600').directive('sideNav', function() {
		return {
			restrict: 'E',
			templateUrl: 'side-nav.html',
			controllerAs: 'sideNav',
			controller: function ($scope, $reactive) {
				$reactive(this).attach($scope);

				this.ipctoggle = true;
				this.cpstoggle = true;
				this.miotoggle = true;

				this.toggleIPC = () => {
					this.ipctoggle = this.ipctoggle === false ? true: false;
				};
				this.toggleCPS = () => {
					this.cpstoggle = this.cpstoggle === false ? true: false;
				};
				this.toggleMIO = () => {
					this.miotoggle = this.miotoggle === false ? true: false;
				};

			}
		}
	});
	
	angular.module('rt600').directive('mainPage', function() {
		return {
			restrict: 'E',
			templateUrl: 'main-page.html',
			controllerAs: 'mainPage',
			controller: function ($scope, $reactive) {
			}
		}
	});

	angular.module('rt600').directive('mio1', function() {
		return {
			restrict: 'E',
			templateUrl: 'mio1.html',
			controllerAs: 'mio1',
			controller: function ($scope, $reactive) {
			}
		}
	});

	angular.module('rt600').directive('ipc', function() {
		return {
			restrict: 'E',
			templateUrl: 'ipc.html',
			controllerAs: 'ipcctrl',
			controller: function($scope, $reactive, ipcService) {
				$reactive(this).attach($scope);

				this.helpers({
					pwrfront: () => {
						let id = "system";
						let result = ipcService.getPwrFront(id);
						return result;
					},
					pwrrear: () => {
						return (IPCDB.findOne({_id:"system"}).pwr_rear);
					}
				});
			}
		}
	});

	angular.module('rt600').service('ipcService', function() {
		this.getPwrFront = (id) => {
			return (IPCDB.findOne({_id:id}).pwr_front);
		};
	});
}
